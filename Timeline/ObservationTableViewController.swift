//
//  TLTableViewController.swift
//  Timeline
//
//  Created by Logan Radloff on 2/18/17.
//  Copyright © 2017 Logan Radloff. All rights reserved.
//

import UIKit
import AudioToolbox
import StoreKit

class ObservationTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ObservationTableViewCellDelegate {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var segmentsAndSegmentLengthButton: UIButton!
    @IBOutlet weak var timerProgressView: UIProgressView!
    @IBOutlet weak var startPauseButton: UIButton!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var countsStackView: UIStackView!

    @IBOutlet weak var taskStackView: UIStackView!
    
    var currentCellIndex: Int {
        return Int((segments - 1) - secondsCount/segmentLength)
    }
    
    var observationOptions : [String?] = ["On Task", "Off Task"]
    
    var segmentList: [Int:IndexSet]!
    
    var timer: Timer!
    var isTimeRunning = false
    var segments = 10
    var segmentLength = 30
    var duration: TimeInterval!
    var secondsCount: Int!
    
    var isPaused: Bool!
    var pauseTime: Int!
    
    var isSoundOn = false
    var isVibrateOn = false
    var isPercentageOn = false
    
    var exportInteractor = ExportInteractor()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        //FOR V1
        //self.navigationItem.rightBarButtonItem = nil
        //FOR V1
        
        segmentList = [:]
        for index in 0...segments-1 {
            segmentList[index] = IndexSet()

        }
        
        duration = TimeInterval(segments*segmentLength)
        secondsCount = Int(duration)
        timeLabel.text = secondsCountToTimeString()
        
        segmentsAndSegmentLengthButton.setTitle("\(segments) x " + secondsToTimeString(secondsToChange: segmentLength) + " segments", for: UIControl.State.normal)
        
        /*Custom Progress View Track Image*/
        UIGraphicsBeginImageContextWithOptions(timerProgressView.frame.size, false, 0.0)
        
        let background = UIBezierPath(rect: CGRect(x: 0, y: 0, width: timerProgressView.frame.width, height: timerProgressView.frame.height))
        UIColor.darkText.setFill()
        background.fill()
        let pvFrame = timerProgressView.frame
        for segmentIndex in 1...segments {
            let tick = UIBezierPath(rect: CGRect(x: (pvFrame.width/CGFloat(segments))*CGFloat(segmentIndex),
                                                 y: 0,
                                                 width: 2,
                                                 height: pvFrame.height))
            UIColor.main.setFill()
            tick.fill()
        }
        
        let trackImage = UIGraphicsGetImageFromCurrentImageContext()
        timerProgressView.trackImage = trackImage
        UIGraphicsEndImageContext()
        
        isPaused = false
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        let cell = self.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! ObservationTableViewCell
        for segNum in 0...observationOptions.count-1 {
            let countLabel = UILabel(frame: CGRect(x: 0, y: 0, width: cell.taskControl.widthForSegment(at: 0), height: 20.5))
            let segCount = Array(segmentList.values).filter{$0.contains(segNum)}.count
            countLabel.text = observationOptions[segNum]! + ": " + String(segCount)
            countLabel.font = UIFont.boldSystemFont(ofSize: 12.0)
            countLabel.textAlignment = NSTextAlignment.center
            countLabel.lineBreakMode = NSLineBreakMode.byTruncatingMiddle
            countsStackView.addArrangedSubview(countLabel)
        }
        updateObservationCounts()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        guard let timer = timer else {
            return
        }
        timer.invalidate()
        if isTimeRunning == true {
            self.startStopTime(startPauseButton)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        startPauseButton.backgroundColor = .positive
        startPauseButton.layer.cornerRadius = 10.0
        startPauseButton.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        resetButton.backgroundColor = .negative
        resetButton.layer.cornerRadius = 10.0
        resetButton.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
        
        timerView.backgroundColor = UIColor.main
        view.backgroundColor = UIColor.main
        segmentsAndSegmentLengthButton.tintColor = UIColor.main
        
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return segments
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ObservationTableViewCell

        // Configure the cell...
        cell.setBackgroundColorIfIsCurrentCell(currentIndex: currentCellIndex, forIndexPath: indexPath)
        
        if cell.delegate == nil {
            cell.delegate = self
        }
        
        cell.indexPath = indexPath
        cell.segmentNumberLabel.text = (indexPath.row + 1).description
        if !cell.hasSegmentedControl {
            cell.setSegmentedControl(items: observationOptions)
        }
        cell.taskControl.selectedSegmentIndexes = NSIndexSet(indexSet: segmentList[indexPath.row]!) as IndexSet
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }

    @IBAction func resetTime(_ sender: Any?) {
        
        if sender != nil {
            let alert = UIAlertController(title: "Choose what to reset", message: "", preferredStyle: UIAlertController.Style.alert)
            let timeAndDataAction = UIAlertAction(title: "Time and observations", style: UIAlertAction.Style.destructive, handler: { (alertAction) in
                self.resetTime()
                self.resetData()
            })
            let timeAction = UIAlertAction(title: "Time only", style: UIAlertAction.Style.default, handler: { (alertAction) in
                self.resetTime()
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (alertAction) in })
            alert.addAction(timeAndDataAction)
            alert.addAction(timeAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func resetTime() {
        if timer != nil {
            timer.invalidate()
        }
        secondsCount = Int(duration)
        timeLabel.text = secondsCountToTimeString()
        isPaused = false
        pauseTime = nil
        isTimeRunning = false
        startPauseButton.setTitle("Start", for: UIControl.State.normal)
        startPauseButton.invalidateIntrinsicContentSize()
        timerProgressView.setProgress(0, animated: false)
        tableView.reloadData()
    }
    
    func resetData() {
        self.segmentList = [:]
        for index in 0...self.segments-1 {
            self.segmentList[index] = IndexSet()
            
        }
        self.updateObservationCounts()
    }

    @IBAction func startStopTime(_ sender: UIButton) {
        /*Puase*/
        if isTimeRunning == true {
            startPauseButton.setTitle("Resume", for: UIControl.State.normal)
            startPauseButton.invalidateIntrinsicContentSize()
            timer.invalidate()
            isPaused = true
            pauseTime = secondsCount
            secondsCount = Int(pauseTime!)
            timeLabel.text = secondsCountToTimeString()
        /*Start or resume from pause*/
        } else {
            //resume
            if isPaused == true {
                secondsCount = Int(pauseTime)
            //start or restart
            } else {
                timerProgressView.setProgress(0, animated: false)
                secondsCount = Int(duration)
            }
            timeLabel.text = secondsCountToTimeString()
            
            isPaused = false
            
            startPauseButton.setTitle("Pause", for: UIControl.State.normal)
            startPauseButton.invalidateIntrinsicContentSize()
            timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
                self.updateTimer()
            RunLoop.current.add(timer, forMode: RunLoop.Mode.common)
            })
        }
        isTimeRunning = !isTimeRunning
    }
    
    func updateTimer() {
        secondsCount = secondsCount - 1
        timeLabel.text = secondsCountToTimeString()

        timerProgressView.setProgress(Float(1-Double(secondsCount)/duration), animated: true)
        
        if secondsCount % segmentLength == segmentLength-1 {
            if isSoundOn {
                AudioServicesPlaySystemSound(1255)
            }
            if isVibrateOn {
                AudioServicesPlaySystemSound(4095)
            }
            tableView.reloadRows(at: [IndexPath(row: currentCellIndex-1, section: 0), IndexPath(row: currentCellIndex, section:0)], with: UITableView.RowAnimation.none)
        }
        
        if secondsCount <= 0 {
            self.resetTime()
            timerProgressView.setProgress(1, animated: false)
            timeLabel.text = secondsToTimeString(secondsToChange:0)
            if isSoundOn {
                AudioServicesPlaySystemSound(1255)
            }
            if isVibrateOn {
                AudioServicesPlaySystemSound(4095)
            }
            SKStoreReviewController.requestReview()
        }
    }
    
    func secondsToTimeString(secondsToChange: Int) -> String{
        let minutes = Int(secondsToChange/60)
        let seconds = Int(secondsToChange) - (minutes * 60)
        return String(format:"%d:%02d",minutes, seconds)
    }
    
    func secondsCountToTimeString() -> String {
        return secondsToTimeString(secondsToChange: secondsCount)
    }
    
    // MARK: - TLTask table view cell delegate
    
    func multiSelect(_ multiSelectSegmentedControl: MultiSelectSegmentedControl!, didChangeValue value: Bool, at index: Int, indexPath: IndexPath) {
        segmentList[indexPath.row] = multiSelectSegmentedControl.selectedSegmentIndexes as IndexSet?
        tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        updateObservationCounts()
    }
    
    func updateObservationCounts() {
        if isPercentageOn {
            for segNum in 0...observationOptions.count-1 {
                let countLabel = countsStackView.arrangedSubviews[segNum] as! UILabel
                let segCount = Array(segmentList.values).filter{$0.contains(segNum)}.count
                let percentage = Float(segCount) / Float(segmentList.count) * 100.0
                countLabel.text = observationOptions[segNum]! + ": " + String(format: "%.1f%%", percentage)
            }
        } else {
            for segNum in 0...observationOptions.count-1 {
                let countLabel = countsStackView.arrangedSubviews[segNum] as! UILabel
                let segCount = Array(segmentList.values).filter{$0.contains(segNum)}.count
                countLabel.text = observationOptions[segNum]! + ": " + String(segCount)
            }
        }
    }
    
    func hasEnteredData() -> Bool {
        for segNum in 0...observationOptions.count-1 {
            let segCount = Array(segmentList.values).filter{$0.contains(segNum)}.count
            if segCount > 0 {
                return true
            }
        }
        return false;
    }
    
    func timerHasRun() -> Bool {
        return secondsCount != Int(duration)
    }
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        if hasEnteredData() || timerHasRun() {
            let alert = UIAlertController(title: "Discard data and reset timer?", message: "It looks like you've recorded some data or the timer has run. Going to the settings screen will discard all data and reset the timer when you return. Are you sure you want to do this?", preferredStyle: UIAlertController.Style.alert)
            let discardAction = UIAlertAction(title: "Discard", style: UIAlertAction.Style.destructive, handler: { (alertAction) in
                _ = self.navigationController?.popViewController(animated: true)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { (alertAction) in })
            alert.addAction(discardAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: nil)
        } else {
            _ = self.navigationController?.popViewController(animated: true)
        }
    }

    @IBAction func exportButtonPressed(_ sender: UIBarButtonItem) {
        self.exportInteractor.presentFileNameAlert(self);
    }
}
