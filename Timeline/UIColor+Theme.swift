//
//  UIColor+Theme.swift
//  Timeline
//
//  Created by Logan Radloff on 9/15/17.
//  Copyright © 2017 Logan Radloff. All rights reserved.
//

import UIKit

extension UIColor {
    static var main : UIColor {
        return UIColor(named: "Main") ?? UIColor(red: 100/255, green: 170/255, blue: 250/255, alpha: 1.0)
    }
    static let lightText = UIColor.white
    static let darkText = UIColor.black
    static let highlight = UIColor.main.withAlphaComponent(0.15)
    static let positive = UIColor(red: 50/255, green: 255/255, blue: 100/255, alpha: 1)
    static let negative = UIColor(red: 255/255, green: 100/255, blue: 100/255, alpha: 1)
}

