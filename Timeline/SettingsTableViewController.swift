//
//  TLTaskAssessmentDetailTableViewController.swift
//  Timeline
//
//  Created by Logan Radloff on 2/19/17.
//  Copyright © 2017 Logan Radloff. All rights reserved.
//

import UIKit
import SafariServices

class SettingsTableViewController: UITableViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, SFSafariViewControllerDelegate {
    let NUM_OF_INTERVALS_KEY = "NUM_OF_INTERVALS"
    let MIN_LENGTH_KEY = "MIN_LENGTH"
    let SEC_LENGTH_KEY = "SECON_LENGTH"
    let CUSTOM_LABEL_ARRAY_KEY = "CUSTOM_LABELS"
    let ALLOWS_MULTISELECT_KEY = "ALLOWS_MULTISELECT"
    let SOUND_KEY = "USES_SOUND"
    let VIBRATE_KEY = "USES_VIBRATE"
    let PERCENTAGE_KEY = "USES_PERCENTAGE"
    
    let IntervalSectionNumber = 0
    let ObservationsLabelsSectionNumber = 1
    let AudioAndHapticFeedbackSectionNumber = 2
    let PercentageSectionNumber = 3
    let MakeDefaultSectionNumber = 4
    
    let defaults = UserDefaults.standard
    
    var assessmentName: String?
    var numberOfSegments: Int = 10
    var minuteSegmentLength = 0
    var secondSegmentLength = 30
    var lengthOfSegments: Int {
        return minuteSegmentLength * 60 + secondSegmentLength
    }
    var lengthOfSegmentsString: String {
        return String(format:"%d:%02d", minuteSegmentLength, secondSegmentLength)
    }
    var assessmentLocation: String?
    var isSoundOn = false
    var isVibrateOn = true
    var isPercentageOn = false
    
    var customObservationLabels: [String] = ["On Task", "Off Task"]
    
    var isIntervalPickerViewRowHidden = true
    
    var isFirstLaunch = true
    
    func boolToString(bool: Bool) -> String {
        if bool {
            return "On"
        } else {
            return "Off"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isEditing = true
        //FOR V1
        let infoButton = UIButton(type: UIButton.ButtonType.infoLight)
        infoButton.addTarget(self, action: #selector(SettingsTableViewController.aboutRowSelected), for: UIControl.Event.touchUpInside)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: infoButton)
        self.navigationItem.prompt = nil
        
        //FOR V1
        
        tableView.keyboardDismissMode = UIScrollView.KeyboardDismissMode.onDrag
        
        if let numIntervals = defaults.object(forKey: NUM_OF_INTERVALS_KEY) {
            numberOfSegments = numIntervals as! Int
        }
        
        if let minuteIntevalLength = defaults.object(forKey: MIN_LENGTH_KEY) {
            minuteSegmentLength = minuteIntevalLength as! Int
        }
        
        if let secondIntervalLength = defaults.object(forKey: SEC_LENGTH_KEY) {
            secondSegmentLength = secondIntervalLength as! Int
        }
        
        if let customLabels = defaults.object(forKey: CUSTOM_LABEL_ARRAY_KEY) {
            customObservationLabels = customLabels as! [String]
        }
        
        if let sound = defaults.object(forKey: SOUND_KEY) {
            isSoundOn = sound as! Bool
        }
        
        if let vibrate = defaults.object(forKey: VIBRATE_KEY) {
            isVibrateOn = vibrate as! Bool
        }
        
        if let percentage = defaults.object(forKey: PERCENTAGE_KEY) {
            isPercentageOn = percentage as! Bool
        }
        
        if isFirstLaunch {
            let assessmentVC = storyboard?.instantiateViewController(withIdentifier: "taskAssessment") as! ObservationTableViewController
            prepareForPush(destination: assessmentVC)
            navigationController?.pushViewController(assessmentVC, animated: false)
            isFirstLaunch = false
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationItem.rightBarButtonItem?.isEnabled = false
        self.navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    func dismissKeyboard() {
        self.view.endEditing(true)
        return
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case IntervalSectionNumber:
            if isIntervalPickerViewRowHidden {
                return 1
            } else {
                return 2
            }
            
        case ObservationsLabelsSectionNumber:
            return 1 + customObservationLabels.count
            
        case AudioAndHapticFeedbackSectionNumber:
            return 2
        default:
            return 1
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: SettingsTableViewCell
        switch indexPath.section {
        case IntervalSectionNumber:
            if indexPath.row == 0 {
                cell = tableView.dequeueReusableCell(withIdentifier: "intervalCell", for: indexPath) as! SettingsTableViewCell
                let minutes = Int(self.lengthOfSegments/60)
                let seconds = Int(self.lengthOfSegments) - (minutes * 60)
                let timeString =  String(format:"%d:%02d",minutes, seconds)
                cell.textLabel?.text = "\(numberOfSegments) x " + timeString
                if isIntervalPickerViewRowHidden {
                    cell.detailTextLabel?.text = "Tap to change"
                    cell.accessoryType = UITableViewCell.AccessoryType.disclosureIndicator
                } else {
                    cell.detailTextLabel?.text = "Tap to close"
                    cell.accessoryType = UITableViewCell.AccessoryType.none
                }
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "segmentPicker", for: indexPath) as! SettingsTableViewCell
                cell.segmentPickerView.delegate = self
                cell.segmentPickerView.dataSource = self
                cell.segmentPickerView.selectRow(numberOfSegments-1, inComponent: 0, animated: false)
                cell.minutesPickerView.delegate = self
                cell.minutesPickerView.dataSource = self
                cell.minutesPickerView.selectRow(minuteSegmentLength, inComponent: 0, animated: false)
                cell.secondsPickerView.delegate = self
                cell.secondsPickerView.dataSource = self
                cell.secondsPickerView.selectRow(secondSegmentLength, inComponent: 0, animated: false)
            }
        case ObservationsLabelsSectionNumber:
            if indexPath.row == customObservationLabels.count {
                cell = tableView.dequeueReusableCell(withIdentifier: "cellWithAddButton", for: indexPath) as! SettingsTableViewCell
                cell.cellLabel.text = "Add label"
            } else {
                cell = tableView.dequeueReusableCell(withIdentifier: "cellWithTextField", for: indexPath) as! SettingsTableViewCell
                cell.cellTextField.delegate = self
                cell.cellTextField.placeholder = "Enter label text"
                cell.cellTextField.text = customObservationLabels[indexPath.row]
                cell.cellTextField.tag = 2 + indexPath.row
            }
        case AudioAndHapticFeedbackSectionNumber:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellWithSwitch", for: indexPath) as! SettingsTableViewCell
            if indexPath.row == 0 {
                cell.cellLabel.text = "Beep"
                cell.cellSwitch.tag = 1
                cell.cellSwitch.isOn = isSoundOn
            } else {
                cell.cellLabel.text = "Vibrate"
                cell.cellSwitch.tag = 2
                cell.cellSwitch.isOn = isVibrateOn
            }
        case PercentageSectionNumber:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellWithSwitch", for: indexPath) as! SettingsTableViewCell
            cell.cellLabel.text = "Show percentage"
            cell.cellSwitch.tag = 3
            cell.cellSwitch.isOn = isPercentageOn
        case MakeDefaultSectionNumber:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellWithButton", for: indexPath) as! SettingsTableViewCell
            cell.centerButton.setTitleColor(UIColor.main, for: .normal)
        default:
            cell = tableView.dequeueReusableCell(withIdentifier: "cellWithSwitch", for: indexPath) as! SettingsTableViewCell
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case IntervalSectionNumber:
            if indexPath.row == 0 {
                isIntervalPickerViewRowHidden = !isIntervalPickerViewRowHidden
                tableView.reloadSections(IndexSet(integer: IntervalSectionNumber), with: UITableView.RowAnimation.automatic)
            }
        case MakeDefaultSectionNumber:
            self.makeDefaultButtonPressed(nil)
        default:
            break
        }
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    @IBAction func cellSwitchDidChange(_ sender: UISwitch) {
        switch sender.tag {
            case 1:
                isSoundOn = sender.isOn
            case 2:
                isVibrateOn = sender.isOn
            case 3:
                isPercentageOn = sender.isOn
            default:
                return
        }
    }
    
    @objc func aboutRowSelected() {
        let svc = SFSafariViewController(url: URL(string: "https://www.insightapp.net")!)
        svc.delegate = self
        svc.preferredBarTintColor = UIColor.main
        //svc.preferredControlTintColor = UIColor.lightText
        self.present(svc, animated: true, completion: nil)
    }
    
    @IBAction func makeDefaultButtonPressed(_ sender: Any?) {
        let confirmAlert = UIAlertController(title: "Confirm Change", message: "Are you sure you want to change the default settings to:\nIntervals: \(numberOfSegments)\nInterval Length: \(lengthOfSegmentsString)\nLabels: \(customObservationLabels)\nBeep: " + boolToString(bool: isSoundOn) + "\nVibrate: " + boolToString(bool: isVibrateOn) + "\nShow percentage: " + boolToString(bool: isPercentageOn), preferredStyle: UIAlertController.Style.alert)
        let yesAction = UIAlertAction(title: "Yes", style: UIAlertAction.Style.default, handler: { (alertAction) in
            self.defaults.set(self.numberOfSegments, forKey: self.NUM_OF_INTERVALS_KEY)
            self.defaults.set(self.minuteSegmentLength, forKey: self.MIN_LENGTH_KEY)
            self.defaults.set(self.secondSegmentLength, forKey: self.SEC_LENGTH_KEY)
            self.defaults.set(self.customObservationLabels, forKey: self.CUSTOM_LABEL_ARRAY_KEY)
            self.defaults.set(self.isSoundOn, forKey: self.SOUND_KEY)
            self.defaults.set(self.isVibrateOn, forKey: self.VIBRATE_KEY)
            self.defaults.set(self.isPercentageOn, forKey: self.PERCENTAGE_KEY)
            
            self.tableView.reloadSections(IndexSet(integer: self.MakeDefaultSectionNumber), with: UITableView.RowAnimation.automatic)
            
            let changedAlert = UIAlertController(title: "Defaults Changed", message: "Your default settings have been updated.", preferredStyle: UIAlertController.Style.alert)
            let okAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil)
            changedAlert.addAction(okAction)
            self.present(changedAlert, animated: true, completion: nil)
            
        })
        let noAction = UIAlertAction(title: "No", style: UIAlertAction.Style.cancel, handler: nil)
        confirmAlert.addAction(yesAction)
        confirmAlert.addAction(noAction)
        self.present(confirmAlert, animated: true, completion: nil)
        
    }
    
    @IBAction func addCustomLabelButtonPressed(_ sender: Any?) {
        if customObservationLabels.count < 10 {
            customObservationLabels.append("")
            let indexPath = IndexPath(row: customObservationLabels.count - 1, section: ObservationsLabelsSectionNumber)
            tableView.insertRows(at: [IndexPath(row: customObservationLabels.count - 1, section: ObservationsLabelsSectionNumber)], with: UITableView.RowAnimation.automatic)
            (tableView.cellForRow(at: indexPath) as! SettingsTableViewCell).cellTextField.becomeFirstResponder()
        } else {
            let alert = UIAlertController(title: "Maximum number of labels", message: "You can only create up to 10 labels.", preferredStyle: UIAlertController.Style.alert)
            let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alertAction) in })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
            case IntervalSectionNumber:
                return "Timer"
            case ObservationsLabelsSectionNumber:
                return "Labels"
            case AudioAndHapticFeedbackSectionNumber:
                return "Audio & Haptic Feedback"
            case PercentageSectionNumber:
                return "Totals"
            case MakeDefaultSectionNumber:
                return "Default Settings"
            default:
                return ""
            }
        }
    
        override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            switch indexPath.section {
            case IntervalSectionNumber:
                if indexPath.row == 0 {
                    return UITableView.automaticDimension
                } else {
                    return CGFloat(254)
                }
            default:
                return UITableView.automaticDimension
            }
        }
    
        override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
            return UITableView.automaticDimension
        }
    
        override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
            if section == self.tableView.numberOfSections - 1 { return UITableView.automaticDimension }
            
            if section == IntervalSectionNumber || section == ObservationsLabelsSectionNumber || section == AudioAndHapticFeedbackSectionNumber || section == PercentageSectionNumber || section == MakeDefaultSectionNumber {
                return UITableView.automaticDimension
            }
            return CGFloat.leastNormalMagnitude
        }
    
    
        override func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
            switch section {
            case IntervalSectionNumber:
                let totalTime = self.numberOfSegments * self.lengthOfSegments
                let minutes = Int(totalTime/60)
                let seconds = Int(totalTime) - (minutes * 60)
                return String(format:"Total Time: %d:%02d",minutes, seconds)
            case ObservationsLabelsSectionNumber:
                return "Labels can get cut off so use abbreviations when possible."
            case PercentageSectionNumber:
                return "Percentages are calculated as the count for label divided by the total number of intervals so they may add up to over 100%."
            case MakeDefaultSectionNumber:
                var intervalsDefault = 10
                if let numIntervals = defaults.object(forKey: NUM_OF_INTERVALS_KEY) {
                    intervalsDefault = numIntervals as! Int
                }
                
                var minuteDefault = 0
                if let minuteIntevalLength = defaults.object(forKey: MIN_LENGTH_KEY) {
                    minuteDefault = minuteIntevalLength as! Int
                }
                
                var secondDefault = 30
                if let secondIntervalLength = defaults.object(forKey: SEC_LENGTH_KEY) {
                    secondDefault = secondIntervalLength as! Int
                }
                
                var customLabelsDefault = ["On Task", "Off Task"]
                if let customLabels = defaults.object(forKey: CUSTOM_LABEL_ARRAY_KEY) {
                    customLabelsDefault = customLabels as! [String]
                }
                
                var soundDefault = false
                if let sound = defaults.object(forKey: SOUND_KEY) {
                    soundDefault = sound as! Bool
                }
                
                var vibrateDefault = true
                if let vibrate = defaults.object(forKey: VIBRATE_KEY) {
                    vibrateDefault = vibrate as! Bool
                }
                
                var percentageDefault = false
                if let percentage = defaults.object(forKey: PERCENTAGE_KEY) {
                    percentageDefault = percentage as! Bool
                }
                
                return "Current defaults:\nIntervals: \(intervalsDefault)\nInterval Length: \(minuteDefault):" + String(format: "%02d", secondDefault) + "\nLabels: \(customLabelsDefault)\nBeep: " + boolToString(bool:soundDefault) + "\nVibrate: " + boolToString(bool:vibrateDefault) + "\nShow percentage: " + boolToString(bool: percentageDefault)
            default:
                return ""
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        switch indexPath.section {
            case ObservationsLabelsSectionNumber:
                if indexPath.row >= customObservationLabels.count {
                    return false
                } else {
                    return true
                }
            default:
                return false
        }
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            if customObservationLabels.count > 2 {
                customObservationLabels.remove(at: indexPath.row)
                tableView.reloadSections(IndexSet(integer: indexPath.section), with: UITableView.RowAnimation.automatic)
            } else {
                let alert = UIAlertController(title: "Minimum number of labels", message: "You must have at least 2 labels.", preferredStyle: UIAlertController.Style.alert)
                let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alertAction) in })
                alert.addAction(alertAction)
                self.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == ObservationsLabelsSectionNumber {
            if indexPath.row >= customObservationLabels.count  {
                return false
            } else {
                return true
            }
        }
        return false
    }
    
    override func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let toMove = customObservationLabels[sourceIndexPath.row]
        customObservationLabels.remove(at: sourceIndexPath.row)
        customObservationLabels.insert(toMove, at: destinationIndexPath.row)
    }
    
    override func tableView(_ tableView: UITableView, targetIndexPathForMoveFromRowAt sourceIndexPath: IndexPath, toProposedIndexPath proposedDestinationIndexPath: IndexPath) -> IndexPath {
        if sourceIndexPath.section != proposedDestinationIndexPath.section ||
            (sourceIndexPath.section == proposedDestinationIndexPath.section && proposedDestinationIndexPath.row >= customObservationLabels.count) {
            var row = 0
            if sourceIndexPath.section <= proposedDestinationIndexPath.section {
                row = customObservationLabels.count - 1
                if proposedDestinationIndexPath.row >= customObservationLabels.count {
                    row = customObservationLabels.count - 1
                }
            }
            return IndexPath(row: row, section: sourceIndexPath.section)
        }
        return proposedDestinationIndexPath
        
    }
    
    // MARK: - Picker view data source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView.tag == 0 {
            return 300
        } else if pickerView.tag == 1 {
            return 61
        } else {
            return 60
        }
    }
    
    // MARK: - Picker view delegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return (row+1).description
        case 1:
            return row.description
        default:
            return String(format: "%02d", row)
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label = view as? UILabel
        
        if label == nil {
            label = UILabel()
        }

        label?.textAlignment = NSTextAlignment.center
        label?.font = UIFont.systemFont(ofSize: 21)
        label?.text = self.pickerView(pickerView, titleForRow: row, forComponent: component)
        
        return label!
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView.tag == 0 {
            self.numberOfSegments = pickerView.selectedRow(inComponent: 0) + 1
        } else if pickerView.tag == 1 {
            self.minuteSegmentLength = pickerView.selectedRow(inComponent: 0)
        } else {
            self.secondSegmentLength = pickerView.selectedRow(inComponent: 0)
        }
        
        tableView.reloadSections(IndexSet(integer: IntervalSectionNumber), with: UITableView.RowAnimation.none)
    }
    
    //MARK: - Text field delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        updateCustomObservationLabelsWith(textField)
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        if textField.tag == 0 {
            assessmentName = textField.text
        } else if textField.tag == 1 {
            assessmentLocation = textField.text
        } else if textField.tag >= 2 {
            updateCustomObservationLabelsWith(textField)
        }
    }
    
    @IBAction func textDidChange(_ sender: UITextField) {
        updateCustomObservationLabelsWith(sender)
    }
    
    func updateCustomObservationLabelsWith(_ textField: UITextField) {
        if textField.tag >= 2 {
            if customObservationLabels.indices.contains(textField.tag - 2) {
                customObservationLabels[textField.tag - 2] = textField.text!
            }
        }
    }
    
    // MARK: - Safari view controller delegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        self.dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Navigation

    @IBAction func cancelButtonPressed(_ sender: UIBarButtonItem) {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    func prepareForPush(destination viewController: ObservationTableViewController) {
        viewController.segments = numberOfSegments
        viewController.segmentLength = lengthOfSegments
        //FOR V1
        viewController.navigationItem.title = "Assessment"
        viewController.navigationItem.prompt = nil
        //FOR V1
        viewController.isSoundOn = isSoundOn
        viewController.isVibrateOn = isVibrateOn
        viewController.isPercentageOn = isPercentageOn
        viewController.observationOptions = customObservationLabels
    }
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "assessmentSettingDone" {
            
            let dVC = segue.destination as! ObservationTableViewController
            prepareForPush(destination: dVC)
        }
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        self.dismissKeyboard()

        if lengthOfSegments == 0 {
            
            let alert = UIAlertController(title: "Change Segment Length", message: "Can not have segment length of 0", preferredStyle: UIAlertController.Style.alert)
            let alertAction = UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (alertAction) in })
            alert.addAction(alertAction)
            self.present(alert, animated: true, completion: nil)
            return false
        }
        
        return true
    }

}

