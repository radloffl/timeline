//
//  TLTableViewCell.swift
//  Timeline
//
//  Created by Logan Radloff on 2/18/17.
//  Copyright © 2017 Logan Radloff. All rights reserved.
//

import UIKit

protocol ObservationTableViewCellDelegate {
    func multiSelect(_ multiSelectSegmentedControl: MultiSelectSegmentedControl!, didChangeValue value: Bool, at index: Int, indexPath: IndexPath)
}

class Observation: UITableViewCell, MultiSelectSegmentedControlDelegate {

    var delegate: ObservationTableViewCellDelegate?
    var indexPath: IndexPath?
    private(set) var hasSegmentedControl = false
    
    @IBOutlet weak var segmentNumberLabel: UILabel!
    @IBOutlet weak var taskControl: MultiSelectSegmentedControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        taskControl.delegate = self
    }
    
    func setBackgroundColorIfIsCurrentCell(currentIndex: Int, forIndexPath indexPath: IndexPath) {
        if currentIndex == indexPath.row {
            contentView.backgroundColor = UIColor.highlight
        } else {
            contentView.backgroundColor = UIColor.lightText
        }
    }
    
    func setSegmentedControl(items: [String?]) {
        if hasSegmentedControl { return }
        taskControl.removeAllSegments()
        for index in 0...items.count-1 {
            taskControl.insertSegment(withTitle: items[index], at: index, animated: false)
        }
        hasSegmentedControl = true
    }
    
    func multiSelect(_ multiSelectSegmentedControl: MultiSelectSegmentedControl!, didChangeValue value: Bool, at index: UInt) {
        delegate?.multiSelect(multiSelectSegmentedControl, didChangeValue: value, at: Int(index), indexPath: indexPath!)
    }
    
}
