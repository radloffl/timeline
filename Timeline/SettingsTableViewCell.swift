//
//  TLTaskAssessmentDetailTableViewCell.swift
//  Timeline
//
//  Created by Logan Radloff on 2/20/17.
//  Copyright © 2017 Logan Radloff. All rights reserved.
//

import UIKit

class SettingsTableViewCell: UITableViewCell {

    @IBOutlet weak var cellTextField: UITextField!
    @IBOutlet weak var cellTextView: UITextView!
    @IBOutlet weak var segmentPickerView: UIPickerView!
    @IBOutlet weak var minutesPickerView: UIPickerView!
    @IBOutlet weak var secondsPickerView: UIPickerView!
    @IBOutlet weak var cellSwitch: UISwitch!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var centerButton: UIButton!
    @IBOutlet weak var addLabelButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
