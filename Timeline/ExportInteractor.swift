//
//  ExportInteractor.swift
//  Timeline
//
//  Created by Logan Radloff on 8/9/17.
//  Copyright © 2017 Logan Radloff. All rights reserved.
//

import UIKit

class ExportInteractor: NSObject, UIDocumentInteractionControllerDelegate {
    
    // Must be a class property, reference is needed to actually save file
    var documentInteractor: UIDocumentInteractionController!
    
    func presentExportController(_ presenter:ObservationTableViewController, fileName: String, exportDate: Date) {
        
        let tmpDirURL = URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true)
        let fileURL = tmpDirURL.appendingPathComponent(fileName).appendingPathExtension("csv")
        
        let csvText = ExportInteractor.createCSV(data: presenter.segmentList, labels: presenter.observationOptions as! [String], numberOfIntervals: presenter.segments, intervalLength: presenter.secondsToTimeString(secondsToChange: presenter.segmentLength), exportDate: exportDate)
        
        do {
            try csvText.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
            
        } catch {
            print("Failed to create file")
            print("\(error)")
        }
        
        documentInteractor = UIDocumentInteractionController(url:fileURL)
        documentInteractor.name = fileName + ".csv"
        documentInteractor.uti = "public.comma-separated-values-text"
        documentInteractor.presentOptionsMenu(from: presenter.navigationItem.rightBarButtonItem!, animated: true)
        
    }
    
    func presentFileNameAlert(_ presenter:ObservationTableViewController) {
        let exportDate = Date()
        let fileNameAlert = UIAlertController(title: "File Name", message: "Name of the file that will be exported.", preferredStyle: .alert)
        var fileName: String?
        var fileNameTF: UITextField?
        fileNameAlert.addTextField { (fileNameTextField) in
            fileNameTF = fileNameTextField
            fileNameTF?.placeholder = ExportInteractor.defaultFileNameFor(exportDate)
            fileNameTF?.selectAll(self);
        }
        let yesAction = UIAlertAction(title: "Export", style: .default) { (alert) in
            fileName = fileNameTF?.text
            if fileName?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ?? false {
                fileName = nil
            }
            self.presentExportController(presenter, fileName: fileName ?? ExportInteractor.defaultFileNameFor(exportDate), exportDate: exportDate)
        }
        let noAction = UIAlertAction(title: "Cancel", style: .cancel) { (alert) in
            return
        }
        fileNameAlert.addAction(yesAction)
        fileNameAlert.addAction(noAction)
        presenter.present(fileNameAlert, animated: true, completion: nil)
    }
    
    private static func createCSV(data: [Int: IndexSet], labels: [String], numberOfIntervals: Int, intervalLength: String, exportDate: Date) -> String {
        var csvString = ""
        csvString.append(createHeader(labels: labels))
        csvString.append(createBody(data: data, numberOfLabels: labels.count))
        csvString.append(createFooter(data: data, numberOfLabels: labels.count))
        csvString.append(createSettings(numberOfIntervals: numberOfIntervals, intervalLength:intervalLength , exportDate: exportDate))
        csvString.append(createNote())
        return csvString
    }
    
    private static func createHeader(labels: [String]) -> String {
        var headerString = "Interval,"
        for label in labels {
            headerString.append("\"" + label + "\",")
        }
        headerString.append("\n")
        return headerString
    }
    
    private static func createBody(data: [Int: IndexSet], numberOfLabels: Int) -> String {
        var bodyString = ""
        for intervalIndex in 0...data.count-1 {
            bodyString.append((intervalIndex+1).description + ",")
            for labelIndex in 0...numberOfLabels-1 {
                if (data[intervalIndex]?.contains(labelIndex))! {
                    bodyString.append("1")
                } else {
                    bodyString.append("0")
                }
                
                if (labelIndex != numberOfLabels-1) {
                    bodyString.append(",")
                }
            }
            bodyString.append("\n")
        }
        return bodyString
    }
    
    private static func createFooter(data: [Int: IndexSet], numberOfLabels: Int) -> String {
        var footerString = "\nTotal,"
        for labelIndex in 0...numberOfLabels-1 {
            let segCount = Array(data.values).filter{$0.contains(labelIndex)}.count
            footerString.append(segCount.description)
            
            if (labelIndex != numberOfLabels-1) {
                footerString.append(",")
            }
        }
        footerString.append("\nPercentage,")
        for labelIndex in 0...numberOfLabels-1 {
            let segCount = Array(data.values).filter{$0.contains(labelIndex)}.count
            let percentage = Float(segCount) / Float(data.keys.count) * 100.0
            footerString.append(String(format: "%.1f%%", percentage))
            
            if (labelIndex != numberOfLabels-1) {
                footerString.append(",")
            }
        }
        footerString.append("\n")
        return footerString
    }
    
    private static func createSettings(numberOfIntervals: Int, intervalLength: String, exportDate: Date) -> String {
        var settingsString = "\nNumber of intervals," + numberOfIntervals.description + "\n"
        settingsString.append("Interval length," + intervalLength.description + "\n")
        

        settingsString.append("Export date," + self.dateTimeString(exportDate))
        settingsString.append("\n")
        return settingsString
    }
    
    private static func createNote() -> String {
        return "\n*Note: The \"Total\" and \"Percentage\" rows come directly from Insight. Changes made to data will not be reflected in this row.\nThe \"Percentage\" row is the total / number of intervals so they may add up to over 100%."
    }
    
    private static func defaultFileNameFor(_ date: Date) -> String {
        return dateTimeString(date) + "-Insight-Observation-Data"
    }
    
    private static func dateTimeString(_ date: Date = Date()) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd-hh:mm"
        return dateFormatter.string(from: date)
    }
}
